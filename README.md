# Kotlin in theory

### Nullability

- Kotlin is a null-safe language
* Null reference was considered by its own inventor, Tony Hoare, the billion dollar mistake https://en.wikipedia.org/wiki/Tony_Hoare
- Kotlin was designed in a way that NPE should not happen unless one of these things happen:
* Some java code causing it [insert MEME here]
* Explicitly calling throw NullPointerException()
* Using the !! operator
[val length = someString!!.length]
* This operator is called Not-null assertion and basically tells the compiler that you’re 100% sure that the expression you’re using in it will never be null.

### Kotlin List/Map/Array/Object/Collection methods

- min() minBy() minWith()
- sort(), sortBy(), sortWith()
- sum() and sumBy()
- groupBy(), sumByDouble(), mapValues(), toSortedMap()

More: https://grokonez.com/kotlin/kotlin-list-methods-min-minby-minwith

### Function Type vs Function literal vs Lambda expression vs Anonymous function 

More: https://blog.kotlin-academy.com/kotlin-programmer-dictionary-function-type-vs-function-literal-vs-lambda-expression-vs-anonymous-edc97e8873e

### Backing field

More: https://medium.com/@nomanr/backing-field-in-kotlin-explained-9f903f27946c

## Part 2: OOP best practices included in Kotlin

### All methods and classes are final, by default

Marco Pivetta: here https://ocramius.github.io/blog/when-to-declare-classes-final/

PROs: less stuffing functionality in existing code via inheritance > helps encapsulation

CONs: final limits flexibility > you can make it extensible at any point in time

More: https://matthiasnoback.nl/2018/09/final-classes-by-default-why/

### No 'new' keyword

Stackoverflow: https://stackoverflow.com/questions/34889793/why-did-kotlin-drop-the-new-keyword

PROs: because there is NO real difference between functions and object construction > nothing prevents a function to allocate an object
* factory functions: these functions create new objects, but they are in no way class constructors
* the new keyword was created because of a negative experience with C\C++, where functions, returning new objects, have to be specially marked (by name conventions) in order not to forget to (manually) free the memory. In a auto-memory-managing language like Java\Kotlin it is not a concern.
* several other languages have no new keyword (Python, Scala, maybe Ceylon) and people who have switched to those languages never seem to miss it

CONs: makes it harder to see the difference between a function call and an object allocation > see syntax
* use of camelCase for names (and avoid underscore in names)
* types start with upper case
* methods and properties start with lower case

### Singleton is created by simply declaring an object

Kotlin docs: https://kotlinlang.org/docs/reference/object-declarations.html#object-declarations

* one line of code https://antonioleiva.com/objects-kotlin/

* how: always has a name following the `object` keyword

* why: an object can’t have any constructor > but init blocks are allowed if some initialization code is needed

More: https://medium.com/@BladeCoder/kotlin-singletons-with-argument-194ef06edd9e + android use case

# Kotlin in code

## Simple web app using Spring

`git clone git@bitbucket.org:andrei-bogdan/springboot-web-app.git`

## Dockerize it

- build image (name):

`docker build -t springboot-web-app-image .`

- run container (background):

`docker-compose up -d`

- visit:

http://localhost:8880