FROM amazoncorretto:11

COPY . /webapp
WORKDIR /webapp

RUN ./gradlew build

EXPOSE 8080

CMD ["java", "-jar", "build/libs/demo-0.0.1-SNAPSHOT.jar"]