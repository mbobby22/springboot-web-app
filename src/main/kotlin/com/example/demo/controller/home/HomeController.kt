package com.example.demo.controller.home

import com.example.demo.controller.BaseController
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class HomeController: BaseController() {
    @GetMapping("/")
    fun blog(model: Model): String {
        model.apply {
            addAttribute("title", "My 1st Kotlin")
        }

        return "home"
    }
}